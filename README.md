# QIS


### Scenario 1
#### Recommendations:
1. Use security group to handle port access for easier management of network access.
2. Ensure the Amazon Linux 2 instances have preconfigured IAM roles for easier management of s3 permissions.
3. Add logging and alarms, and notificiations using VPC Flows, Cloudwatch and SNS with relevant thresholds. This provides users with visibility on network flow and application logs, and allow users to be immediately notified of unusual behaviour.
4. Add a WAF for security against common attacks, and rate-limit known bad actors.
5. Automate the any updates to the WAF based on the alarms set.
6. Consider multi-AZs for AZ-failure rollovers.

---

### Scenario 2:
#### Assumptions:
- That a kubernetes cluster has already been setup
- That the images are built into a target repo (current repo values are "repo-name" placeholders)
- The api app is needed for the frontend nginx to talk to the mysql (the .js script calls the api, which writes into the database)
- helm repos for Prometheus and Grafana have been added

### Steps
1. Build the Docker images
	- Docker build -f nginx-app.Dockerfile -t repo-name/nginx-app .
	- Docker build -f api.Dockerfile -t repo-name/api-app .
	- Docker build -f mysql.Dockerfile -t repo-name/mysql-db .
	
2. Create the Kubernetes deployments
	- kubectl create -f nginx-deployment.yml
	- kubectl create -f api-deployment.yml
	- kubectl create -f mysql=deployment.yml

3. Create the Kubernetes services
	- kubectl create -f nginx-service.yml
	- kubectl create -f api-app-service.yml
	- kubectl create -f mysql=service.yml

4. Install the monitoring services
	- helm install prometheus \<helm-repo\>/prometheus
	- helm install grafana \<helm-repo\>/grafana
