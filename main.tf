provider "aws" {
  region = "ap-southeast-1"
}

# sg for ALB
resource "aws_security_group" "alb_sg" {
  name        = "alb_sg"
  description = "Security group for ALB"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# sg for fleet
resource "aws_security_group" "nginx_sg" {
  name        = "nginx_sg"
  description = "Security group for Nginx instances"

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.alb_sg.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Private Subnet
resource "aws_subnet" "private_subnet" {
  vpc_id                  = "vpc-03f8995d6f424e28f"
  cidr_block              = "172.31.48.0/24"
  availability_zone       = "ap-southeast-1a"
  map_public_ip_on_launch = false
}

# Launch Template
resource "aws_launch_template" "server_fleet_A_lt" {
  name_prefix            = "server-fleet-A-"
  instance_type          = "t2.micro"
  image_id               = "ami-0eb4694aa6f249c52"
  vpc_security_group_ids = [aws_security_group.nginx_sg.id]
  user_data              = <<-EOF
              #!/bin/bash
              yum install -y nginx
              aws s3 cp s3://qis-bl-tech/index.html /usr/share/nginx/html/index.html
              service nginx start
              EOF

  iam_instance_profile {
    name = "allowEC2toReadS3"
  }
}

# ASG
resource "aws_autoscaling_group" "server_fleet_A" {
  name             = "qis-asg"
  desired_capacity = 3
  min_size         = 3
  max_size         = 3

  launch_template {
    id      = aws_launch_template.server_fleet_A_lt.id
    version = "$Latest"
  }

  vpc_zone_identifier = [aws_subnet.private_subnet.id]
}

# ASG Lifecycle Hook
resource "aws_autoscaling_lifecycle_hook" "asg_lc_hook" {
  name                  = "lifecycle-hook"
  autoscaling_group_name = aws_autoscaling_group.server_fleet_A.name
  default_result        = "CONTINUE"
  lifecycle_transition   = "autoscaling:EC2_INSTANCE_LAUNCHING"
  heartbeat_timeout      = 300
}

# ALB
resource "aws_lb" "nginx_alb" {
  name               = "nginx-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb_sg.id]
  subnets            = ["subnet-0b23cbb680e325b2c", "subnet-0ecf026b80af6797c"]

}

resource "aws_lb_listener" "nginx_alb_listener" {
  load_balancer_arn = aws_lb.nginx_alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.server_fleet_A_tg.arn
  }
}

# ASG Target Groups
resource "aws_lb_target_group" "server_fleet_A_tg" {
  name        = "server-fleet-A-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "vpc-03f8995d6f424e28f"
}

# Attach ASG to tg
resource "aws_lb_target_group_attachment" "server_fleet_A_attachment" {
  target_group_arn = aws_lb_target_group.server_fleet_A_tg.arn
  target_id        = aws_autoscaling_group.server_fleet_A.id
  port             = 80
}
