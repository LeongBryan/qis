CREATE DATABASE counter;
USE counter;

CREATE TABLE counter (
  id INT NOT NULL AUTO_INCREMENT,
  `count` VARCHAR(1) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO counter (`count`) VALUES ('+');
INSERT INTO counter (`count`) VALUES ('-');
