from flask import Flask, render_template, request
import mysql.connector
from flask_cors import CORS

app = Flask(__name__)
CORS(app, origins='*')

# Database connection configuration
mydb = mysql.connector.connect(
  host="mysql-service",
  user="root",
  password="password",
  database="counter"
)

@app.route('/')
def index():
    return render_template('app_index.html')

@app.route('/insert_plus', methods=['POST'])
def insert_plus():
    mycursor = mydb.cursor()
    sql = "INSERT INTO counter (count) VALUES ('+')"  # Corrected SQL query
    mycursor.execute(sql)
    mydb.commit()  # Commit the transaction after executing the query
    return render_template('app_index.html')

@app.route('/insert_minus', methods=['POST'])
def insert_minus():
    mycursor = mydb.cursor()
    sql = "INSERT INTO counter (count) VALUES ('-')"  # Corrected SQL query
    mycursor.execute(sql)
    mydb.commit()  # Commit the transaction after executing the query
    return render_template('app_index.html')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
